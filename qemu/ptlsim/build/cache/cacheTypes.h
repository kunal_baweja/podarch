/*
 * DO NOT MODIFY
 * This file is atuomatically generated by Marss Builder.
 * If you want to make any changes to this configuration please make
 * changes to '.conf' file which was used to generate this.
 * 
 * conf file:  
 */


enum {
	L2_256K,
	L3_4M,
	L1_64K,
};

namespace Memory {
    struct CacheLinesBase;
    CacheLinesBase* get_cachelines(int type);
};
